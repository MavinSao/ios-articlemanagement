//
//  ArticleService.swift
//  ArticleManagement
//
//  Created by Mavin on 12/24/19.
//  Copyright © 2019 Mavin. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
class ArticleService {
    let MAIN_URL = "http://110.74.194.124:15011"
    let header = [
           "Content-Type":"application/json",
           "Accept":"application/json"
       ]
    
    func fetchArticle(page : Int,limit : Int, completion : @escaping ([Article],Pagination)->()){
        
        var articles = [Article]()
        
        Alamofire.request(MAIN_URL+"/v1/api/articles?page=\(page)&limit=\(limit)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if response.result.isSuccess {
                let json = try? JSON(data: response.data!)
                
                for jsonData in json!["DATA"].array! {
                    articles.append(Article(json: jsonData))
                }
                
                let pagination = Pagination(json: json!["PAGINATION"])
                completion(articles,pagination)
            }
        }
    }
    func deleteArticle(id : Int, completion : @escaping (_ message : String)->()){
           Alamofire.request(MAIN_URL + "/v1/api/articles/\(id)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
               if response.result.isSuccess {
                   completion("Delete Success")
               }else{
                   completion("Fail to Delete")
            }
           }
       }
    
    func postImage(imageData : Data, completion: @escaping (_ urlImage : String, _ message : String)->()){
        Alamofire.upload(multipartFormData: { (multiform) in
            multiform.append(imageData, withName: "FILE", fileName: ".jpg", mimeType: "image/jpeg")
        }, to: MAIN_URL + "/v1/api/uploadfile/single") { (encodingResult) in
            switch encodingResult {
            case .success(request: let request, streamingFromDisk: _, streamFileURL: _) :
                request.responseJSON { (response) in
                    if response.result.isSuccess{
                        let json = try! JSON(data: response.data!)
                        let urlImage = json["DATA"].stringValue
                        completion(urlImage, "upload success")
                    }
                    
                }
            case .failure(_):
                        completion("", "upload fail")
            }
        }
    }
    
    func postArticle(parameter : [String:Any], completion: @escaping (_ message : String)->()){
        Alamofire.request(MAIN_URL + "/v1/api/articles", method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if response.result.isSuccess {
                completion("post successfully")
            }else{
                completion("post fail")
            }
        }
    }
    func updateArticle(parameter : [String:Any],id : Int, completion: @escaping (_ message : String)->()){
        Alamofire.request(MAIN_URL + "/v1/api/articles/\(id)", method: .put, parameters: parameter, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if response.result.isSuccess {
                completion("Update successfully")
            }else{
                completion("Update fail")
            }
        }
    }
}
