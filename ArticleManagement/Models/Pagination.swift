//
//  Pagination.swift
//  ArticleManagement
//
//  Created by Mavin on 12/25/19.
//  Copyright © 2019 Mavin. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Pagination{
    var page,limit,totalCount,totalPages:Int?
    init(json:JSON) {
        self.page = json["PAGE"].int
        self.limit = json["LIMIT"].int
        self.totalCount = json["TOTAL_COUNT"].int
        self.totalPages = json["TOTAL_PAGES"].int
    }
}
