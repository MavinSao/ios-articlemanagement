//
//  ArticleModel.swift
//  ArticleManagement
//
//  Created by Mavin on 12/25/19.
//  Copyright © 2019 Mavin. All rights reserved.
//

import Foundation
struct ArticleModel {
    
    var id:Int?
    var title,description,createdDate,status:String?
    var category:String
    var image : String?
    var author : String?
    var views : Int?
    
    
    var allAuthor =  ["Mavin Sao", "Kimleang Kea", "Sengly Sun"]
    
    init(article : Article) {
        self.id = article.id
        self.title = article.title
        self.description = article.description
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyyMMddHHmmss"
        
        if let date = dateFormat.date(from: article.createdDate!) {
            let newDate = DateFormatter()
            newDate.dateFormat = "dd-MMM-yyyy"
            self.createdDate = newDate.string(from: date)
        }
        
        self.status = article.status
        self.category = article.category?.name ?? "No Title"
        self.author = self.allAuthor.randomElement()
        self.views = Int.random(in: 0 ..< 200)
        self.image = article.image
    }
    
   
   
}
