//
//  Articles.swift
//  ArticleManagement
//
//  Created by Mavin on 12/25/19.
//  Copyright © 2019 Mavin. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Article {
    var id:Int?
    var title,description,createdDate,status,image:String?
    var category:Category?
    
    init(json:JSON) {
        self.id = json["ID"].int
        self.title = json["TITLE"].string
        self.description = json["DESCRIPTION"].string
        self.createdDate = json["CREATED_DATE"].string
        self.status = json["STATUS"].string
        self.category = Category(json: json["CATEGORY"])
        self.image = json["IMAGE"].string
    }
}
