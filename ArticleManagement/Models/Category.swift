//
//  Category.swift
//  ArticleManagement
//
//  Created by Mavin on 12/25/19.
//  Copyright © 2019 Mavin. All rights reserved.
//

import Foundation
import SwiftyJSON
struct Category {
    
    var id:Int?
    var name:String?
    init(json:JSON) {
        self.id = json["ID"].int
        self.name = json["NAME"].string
    }
    
}
