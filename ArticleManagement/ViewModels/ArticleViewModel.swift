//
//  ArticleViewModel.swift
//  ArticleManagement
//
//  Created by Mavin on 12/24/19.
//  Copyright © 2019 Mavin. All rights reserved.
//

import Foundation

class ArticleViewModel {
    private let service = ArticleService()
    func fetchArticle(page:Int,limit:Int,completion : @escaping ([ArticleModel],Pagination)->()){
        
        service.fetchArticle(page: page, limit: limit) { (articles,pagination) in
            var newArticleModel = [ArticleModel]()
            newArticleModel = articles.compactMap(ArticleModel.init)
            completion(newArticleModel,pagination)
        }
    }
    func deleteArticle(id : Int, completion : @escaping (_ message : String)->()){
        service.deleteArticle(id: id) { (msg) in
            completion(msg)
        }
    }
    func postArticle(parameter : [String:Any], completion : @escaping (_ message : String)->()){
        service.postArticle(parameter: parameter) { (msg) in
            completion(msg)
        }
    }
    
    func postUpdate(imageData : Data,parameter : [String:Any],isUpdate : Bool, id : Int,completion: @escaping (_ message : String)->()){
        var parameters = parameter
        service.postImage(imageData: imageData) { (url, msg) in
            if isUpdate {
                parameters["IMAGE"] = url
                self.service.updateArticle(parameter: parameters, id: id) { (msg) in
                    completion(msg)
                }
            }else{
                 parameters["IMAGE"] = url
                self.service.postArticle(parameter: parameters) { (msg) in
                    completion(msg)
                }
            }
        }

    }
}
