//
//  ArticleViewCell.swift
//  ArticleManagement
//
//  Created by Mavin on 12/25/19.
//  Copyright © 2019 Mavin. All rights reserved.
//

import UIKit
import Kingfisher

class ArticleViewCell: UITableViewCell {
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var viewLabel: UILabel!
    @IBOutlet weak var createdDate: UILabel!
    @IBOutlet weak var authorName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func prepare(article : ArticleModel){
        self.title.text = article.title!.trimmingCharacters(in: .whitespacesAndNewlines)
        self.createdDate.text = article.createdDate
        self.authorName.text = article.author
        self.viewLabel.text = ("\(article.views ?? 0)")
        if let imgUrl = article.image {
             self.thumbnail.kf.setImage(with: URL(string: imgUrl), placeholder: UIImage(named: "default"), options: nil)
        }else{
             self.thumbnail.image = UIImage(named: "default")
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
