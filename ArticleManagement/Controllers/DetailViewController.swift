//
//  DetailViewController.swift
//  ArticleManagement
//
//  Created by Mavin on 12/25/19.
//  Copyright © 2019 Mavin. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController,UIGestureRecognizerDelegate,Message {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textTitle: UILabel!
    @IBOutlet weak var textDatetime: UILabel!
    @IBOutlet weak var textView: UILabel!
    @IBOutlet weak var textType: UILabel!
    @IBOutlet weak var textDescription: UILabel!
    
    func didSent(article: ArticleModel) {
        self.titleText = article.title ?? ""
        self.descriptionText = article.description ?? ""
        self.dateTime = article.createdDate ?? ""
        self.type = article.category
        self.views = article.views ?? 0
        self.urlImage = article.image ?? ""
    }
    
    var titleText = ""
    var descriptionText = ""
    var dateTime = ""
    var type = ""
    var views : Int?
    var id : Int?
    var urlImage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Detail"
        self.textTitle.text = titleText.trimmingCharacters(in: .whitespacesAndNewlines)
        self.textDescription.text = descriptionText.trimmingCharacters(in: .whitespacesAndNewlines)
        self.textDatetime.text = dateTime
        self.textView.text = ("\(views ?? 0)")
        self.textType.text = type
        self.imageView.kf.setImage(with: URL(string: urlImage), placeholder: UIImage(named: "default"), options: nil)
        // Do any additional setup after loading the view.
        
        //LongPress
        imageView.isUserInteractionEnabled = true
        let longpress : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        longpress.minimumPressDuration = 0.5
        longpress.delegate = self
        self.imageView.addGestureRecognizer(longpress)
        
        
    }
    @objc func handleLongPress(gestureRecognizer : UILongPressGestureRecognizer){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let savePhoto = UIAlertAction(title: "Save Photo", style: .default) { (_) in
            let image = self.imageView.image!
            
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
            
            self.dismiss(animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (_) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(savePhoto)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
