//
//  ViewController.swift
//  ArticleManagement
//
//  Created by Mavin on 12/24/19.
//  Copyright © 2019 Mavin. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var delegate : Message?
    
    let articleViewModel = ArticleViewModel()
    
    var articles = [ArticleModel]()
    
    @IBOutlet weak var tableView: UITableView!
    
    var page = 1
    var limit = 15
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ArticleViewCell
        cell.prepare(article: articles[indexPath.row])
        return cell
    }
    
   func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
    let updateArticle = UIContextualAction(style: .normal, title: nil) { (action, view, _) in
            let addUpdateView = self.storyboard?.instantiateViewController(identifier: "addUpdateView") as! AddUpdateViewController
            self.delegate = addUpdateView
            self.delegate?.didSent(article: self.articles[indexPath.row])
            self.navigationController?.pushViewController(addUpdateView, animated: true)
        }
        updateArticle.image = UIImage(systemName: "square.and.pencil")
    updateArticle.backgroundColor = .brown
        return UISwipeActionsConfiguration(actions: [updateArticle])
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            _ = SweetAlert().showAlert("Warning!!", subTitle: "Do you want to delete it?", style: .warning, buttonTitle: "Delete", buttonColor: .red,otherButtonTitle: "No",otherButtonColor: .purple, action: { (isDelete) in
                        if isDelete {
                            self.articleViewModel.deleteArticle(id: self.articles[indexPath.row].id!) { (msg) in
                            self.loadArticle()
                            }
                        }else{
                            print("Not Deleted")
                        }
               })
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailViewController = storyboard?.instantiateViewController(identifier: "detail") as! DetailViewController
        
        self.delegate = detailViewController
        self.delegate?.didSent(article: self.articles[indexPath.row])
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
           if indexPath.row == self.articles.count-1 {
               page += 1
               articleViewModel.fetchArticle(page: page, limit: limit) { (articles, pagination) in
               self.articles += articles
               self.tableView.reloadData()
            }
           }
    } 

    override func viewWillAppear(_ animated: Bool) {
        loadArticle()
    }
    
    
    func loadArticle() {
        page = 1
        articleViewModel.fetchArticle(page: page, limit: limit) { (articles, pagination) in
            self.articles = articles
            self.tableView.reloadData()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register cell
        self.tableView.register(UINib(nibName: "ArticleViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        //Config color on navigationbar
        navigationController?.navigationBar.backgroundColor = UIColor.purple
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .purple
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]

        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.compactAppearance = appearance
        navigationController?.navigationBar.tintColor = .white
        
        //To refresh
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tableView.refreshControl = refreshControl
    }
    
    @objc func refresh(refreshControl: UIRefreshControl){
        loadArticle()
        // somewhere in your code you might need to call:
        refreshControl.endRefreshing()
    }
    
   
    
    

}

