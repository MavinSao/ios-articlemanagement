//
//  AddUpdateViewController.swift
//  ArticleManagement
//
//  Created by Mavin on 12/25/19.
//  Copyright © 2019 Mavin. All rights reserved.
//

import UIKit
import JJFloatingActionButton
import Kingfisher

protocol Message {
    func didSent(article:ArticleModel)
}

class AddUpdateViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,Message {
    @IBOutlet weak var addUpdateButton: UIButton!
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var textDescription: UITextView!
    @IBOutlet weak var textTitle: UITextField!
    
    var titleText = ""
    var descriptionText = ""
    var id : Int?
    var urlImage = ""
    var isUpdate = false
    var imagePicker : UIImagePickerController!
    let articleViewModel = ArticleViewModel()
    var alert = SweetAlert()
    
    func didSent(article: ArticleModel) {
        self.isUpdate = true
        self.titleText = article.title ?? ""
        self.descriptionText = article.description ?? ""
        self.id = article.id
        self.urlImage = article.image ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customButtnAddUpdate()
        
      //image Picker
    
        let didTouch = UITapGestureRecognizer(target: self, action: #selector(browImage))
        thumbnail.addGestureRecognizer(didTouch)
        thumbnail.isUserInteractionEnabled = true
        
    }
    
    func customButtnAddUpdate() {
        
        addUpdateButton.backgroundColor = .purple
        if isUpdate{
            addUpdateButton.setImage(UIImage(systemName: "pencil"), for: .normal)
        }else{
            addUpdateButton.setImage(UIImage(systemName: "plus"), for: .normal)
        }
        addUpdateButton.layer.cornerRadius = addUpdateButton.frame.height / 2
        addUpdateButton.layer.shadowOpacity = 0.25
        addUpdateButton.layer.shadowRadius = 5
        addUpdateButton.layer.shadowOffset = CGSize(width: 0, height: 10)
        
        UIView.animate(withDuration: 0.5, animations: {
           self.addUpdateButton.transform = CGAffineTransform.init(translationX: 0, y: -50)
          }, completion: nil)
        
          if let window = UIApplication.shared.keyWindow {
            addUpdateButton.translatesAutoresizingMaskIntoConstraints = false
              window.addSubview(addUpdateButton)
            addUpdateButton.trailingAnchor.constraint(equalTo: window.trailingAnchor,constant: -10).isActive = true
            addUpdateButton.bottomAnchor.constraint(equalTo: window.bottomAnchor).isActive = true
            addUpdateButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
            addUpdateButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
          }
        
    }
    
    func addUpdate(){
        let imgData = self.thumbnail.image?.jpegData(compressionQuality: 1)
                  let parameter : [String:Any] = ["TITLE" : self.textTitle.text as Any ,
                                                  "DESCRIPTION" : self.textDescription.text as Any]
                  self.articleViewModel.postUpdate(imageData: imgData!, parameter: parameter, isUpdate: self.isUpdate, id: self.id ?? 0) { (msg) in
                     
                    if self.isUpdate{
                        _ = SweetAlert().showAlert("SUCCESS!!", subTitle: "Your updating have been successfuly", style: .success, buttonTitle: "OK", buttonColor: .purple, action: { (_) in
                            self.navigationController?.popViewController(animated: true)
                        })
                    }else{
                        _ = SweetAlert().showAlert("SUCCESS!!", subTitle: "Your article have been added", style: .success, buttonTitle: "OK", buttonColor: .purple, action: { (_) in
                            self.navigationController?.popViewController(animated: true)
                        })

                    }
                    
                      
      }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isUpdate {
            self.textTitle.text = titleText.trimmingCharacters(in: .whitespacesAndNewlines)
            self.textDescription.text = descriptionText.trimmingCharacters(in: .whitespacesAndNewlines)
            self.thumbnail.kf.setImage(with: URL(string: urlImage), placeholder: UIImage(named: "default"), options: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         UIView.animate(withDuration: 0.5, animations: {
                     self.addUpdateButton.transform = CGAffineTransform.init(translationX: 100, y: -50)
                    }, completion: { (_) in
                       self.addUpdateButton.removeFromSuperview()
                    })
    }
    
    @IBAction func addUpdateClicked(_ sender: Any) {
        addUpdate()
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[.originalImage] as! UIImage
        thumbnail.image = image
        dismiss(animated: true, completion: nil)
    }
    
   @objc func browImage(){
        let chooseOption = UIAlertController(title: "Choose Photo", message: nil, preferredStyle: .actionSheet)
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (_) in
            self.dismiss(animated: true, completion: nil)
            self.chooseBrowsOption(option: .camera)
        }
        let fromGalary = UIAlertAction(title: "Choose from Library", style: .default) { (_) in
            self.dismiss(animated: true, completion: nil)
            self.chooseBrowsOption(option: .photoLibrary)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            self.dismiss(animated: true, completion: nil)
        }
        chooseOption.addAction(takePhoto)
        chooseOption.addAction(fromGalary)
        chooseOption.addAction(cancel)
    
        present(chooseOption, animated: true, completion: nil)
    }
    
    func chooseBrowsOption(option : UIImagePickerController.SourceType){
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = option
        self.present(self.imagePicker, animated: true, completion: nil)
    }

}
